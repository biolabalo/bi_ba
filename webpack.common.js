const webpack = require('webpack');
const path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");

const config = {
  entry: './src/index.js',
  plugins:[
new HtmlWebPackPlugin({
  template : "./src/index.html"
})
  ],
  module: {
    rules: [

      {
        "test": /\.js$/,
        "exclude": /node_modules/,
        "use": {
            "loader": "babel-loader",
            "options": {
                "presets": [
                  "@babel/preset-env"
                ]
            }
        }
    },
      {
        test: /\.png$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              mimetype: 'image/png'
            }
          }
        ]
      },
      {
        test: /\.(jpg|jpeg|gif|png|svg|webp)$/,
        use: {
          loader: "file-loader",
          options: {
            esModule: false,
            name: "[name].[hash].[ext]",
            outputPath: "imgs"
            }
        }
      },
      {
        test: /\.html$/,
        use: [{
          loader: 'html-loader'
        }]
      },
    ]
  }
};

module.exports = config;