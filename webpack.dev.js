const common  = require("./webpack.common");
const path = require('path');
const merge = require("webpack-merge");
var HtmlWebpackPlugin = require("html-webpack-plugin");

const config = merge(common,{
  mode: "development",
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'main.js'
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.html"
    })
  ],
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          "style-loader", 
          "css-loader", 
          "sass-loader"
        ]
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      }
    ]
}
});

module.exports = config;