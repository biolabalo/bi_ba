export const intializeStore = () =>{
    window.addEventListener('DOMContentLoaded', (event) => {
        if(!localStorage.getItem('userData'))
        localStorage.setItem('userData', JSON.stringify({
            name: "Jessica parker",
            website: "www.biola.com",
            phoneNumber:"07018091960",
            address:"23 bisi olatunji street ojodu berger",
        }));

    });
}

export const rehydrate_UI_ON_PAGE_LOAD = (
    userName,
    user_address,
    phone_Number, 
    userNameTab,
    addressTab,
    websiteTab,
    phoneNumberTab) => {

    if(localStorage.getItem('userData')){
        const {name, phoneNumber,website ,address } = JSON.parse(localStorage.getItem('userData'));

        userName.innerHTML = name;
        user_address.innerHTML = address;
        phone_Number.innerHTML = phoneNumber;
        phoneNumberTab.innerHTML = phoneNumber;
        userNameTab.innerHTML = name;
        addressTab.innerHTML = address;
        websiteTab.innerHTML = website;
    }
}
