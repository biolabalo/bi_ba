function showPanel(panelIndex, tabButtons, tabPanels) {
    tabButtons.forEach(function(node){
        node.style.backgroundColor="";
        node.style.color="";
        node.style.borderBottom="";
    });
    tabButtons[panelIndex].style.borderBottom  =  "3px solid rgb(25,135,219)";
    tabButtons[panelIndex].style.color="#767779";
    tabPanels.forEach(function(node){
        node.style.display="none";
    });
    tabPanels[panelIndex].style.display="block";
    tabPanels[panelIndex].style.backgroundColor="white";
}

export default showPanel;