import "./styles/main.scss";
import "./styles/main.scss";

import showPanel from "./scripts/tabHandler";
import {intializeStore, rehydrate_UI_ON_PAGE_LOAD} from "./scripts/localStorageHandler";

 
export const tabButtons=document.querySelectorAll(".tabContainer .buttonContainer button");
export const tabPanels=document.querySelectorAll(".tabContainer  .tabPanel");
export const aboutBtn = document.getElementById("about-btn");
export const settingsBtn = document.getElementById("settings-btn");
export const userName =document.getElementById("username-header");
export const userNameTab =document.getElementById("username-tab");
export const address = document.getElementById("address-header");
export const addressTab =document.getElementById("home-address");
export const phoneNumber = document.getElementById("phonenumber-header");
export const phoneNumberTab = document.getElementById("tab-phonenumber")
export const websiteTab = document.getElementById("website");

intializeStore();

rehydrate_UI_ON_PAGE_LOAD(
    userName,
    address, 
    phoneNumber, 
    userNameTab, 
    addressTab,
    websiteTab,
    phoneNumberTab);

showPanel(0, tabButtons, tabPanels);

aboutBtn.addEventListener("click", ()=>{
    showPanel(0,tabButtons, tabPanels)
});

settingsBtn.addEventListener("click", ()=>{
showPanel(1, tabButtons, tabPanels)
});

window.showPopUp = (id, valueHolder, inputField) => {
    document.getElementById(inputField).value = document.getElementById(valueHolder).innerHTML;
    document.getElementById(id).classList.toggle("show");
}


window.update_UI_Values = (e, target) => {
const nodeItems = [...(document.getElementsByClassName(target))];
for (var eachNode of nodeItems) {
   eachNode.innerHTML =  e.target.value
  }
}

window.updateLocalStorage = (e, inputfield, key , popUp) => {
e.preventDefault();
const state = JSON.parse(localStorage.getItem('userData'));
const newData = document.getElementById(inputfield).value;

if(!newData) return alert('field cannot be empty')

state[key] = newData;
localStorage.setItem('userData', JSON.stringify(state))
document.getElementById(popUp).classList.toggle("show");

}

window.closePopUp = popUp => document.getElementById(popUp).classList.toggle("show");